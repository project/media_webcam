
/**
 * @file opalcanvas/includes/js/opalcanvas.js
 *
 * Display the OpalCanvas editor.
 */

(function($) {

  var opalCanvas = opalCanvas || {};
  Drupal.settings.opalCanvas = Drupal.settings.opalCanvas || {};

  // Define the opalCanvas object.
  Drupal.opalCanvas = Drupal.opalCanvas || {};

  /**
   * Display the Drupal throbber.
   *
   * @param $element
   *  The jQuery element to append the throbber to.
   * @param boolean throbber
   *  If TRUE, then display the throbber element.
   */
  opalCanvas.throbber = function($element, throbber) {
    if (throbber) {
      $element.after('<div class="ahah-progress ahah-progress-throbber"><div class="throbber">&nbsp;</div></div>');
    }
    else {
      $element.siblings('.ahah-progress').remove();
    }
  }


  /**
   * This is an empty drawing step.
   */
  function opalCanvasStack() {
    // Arrays of x/y positions.
    this.x = new Array();
    this.y = new Array();
    // Array of Boolean values of whether to drag from previous position or not.
    this.drag = new Array();
    // The color of the stroke.
    this.strokeStyle = new Array();
    // The width of the line.
    this.lineWidth = new Array();
  }

  /**
   * This is an empty stack of steps.
   */
  function opalCanvasSection() {
    // This contains the stack of steps.
    this.stack = new Array();

    // Add a new empty step to the stack.
    this.newStack = function() {
      return this.stack.push(new opalCanvasStack());
    }

    // Push a step onto the stack.
    this.pushStack = function(stack) {
      return this.stack.push(stack);
    }

    // Pop a step from the stack.
    this.popStack = function() {
      return this.stack.pop();
    }

    // Empty the stack entirely.
    this.emptyStack = function() {
      this.stack = new Array();
    }
  }

  /**
   * Undo an entire section of steps.
   */
  opalCanvas.undo = function($canvas) {
    // Pop a section of steps from the canvas stack.
    var stack = $canvas.data('section').popStack();

    // Do we have a section to undo?
    if (stack) {
      // Store the section of steps for possible later redo.
      $canvas.data('undo').pushStack(stack);
      // Redraw the canvas sans the popped section.
      opalCanvas.redraw($canvas);
    }
  }

  /**
   * Redo a previously undone section of steps.
   */
  opalCanvas.redo = function($canvas) {
    // Pop a section of steps from the undo stack.
    var stack = $canvas.data('undo').popStack();

    // Do we have a section to redo?
    if (stack) {
      // Add the section back to the cavnas.
      $canvas.data('section').pushStack(stack);
      // Redraw the canvas with the redone section of steps.
      opalCanvas.redraw($canvas);
    }
  }

  /**
   * Register a mouse click to the canvas.
   */
  opalCanvas.addClick = function($canvas, x, y, dragging)
  {
    // Grab the latest section to add our step.
    var stack = $canvas.data('section').popStack();
    // If we don't have a stack, then create one now.
    if (stack === undefined) {
      stack = new opalCanvasStack();
    }
    // Add our position.
    stack.x.push(x);
    stack.y.push(y);
    // Are we dragging from a previous position?
    stack.drag.push(dragging);
    // Add the color from the colorpicker.
    stack.strokeStyle.push($('#' + Drupal.settings.opalCanvas.element[0] + ' input.opalcanvas-color').css('background-color'));
    // Add the size to our pen.
    stack.lineWidth.push($('#' + Drupal.settings.opalCanvas.element[0] + ' .opalcanvas-size').val());
    // Add the section of steps to the canvas.
    $canvas
      .data('section')
      .pushStack(stack);
    // Empty the undo stack since we're drawing more now.
    $canvas
      .data('undo')
      .emptyStack();
  }

  /**
   * Draw the current canvas.
   */
  opalCanvas.redraw = function($canvas) {
    // Get the DOM object of the canvas.
    var canvas = $canvas.get(0);
    // Get the HTML5 context of the canvas.
    var context = $canvas.data('context');
    // Clear the canvas.
    canvas.width = canvas.width;

    // Set our pen style.
    // @TODO: Add to stack w/ selector.
    context.lineJoin = "round";

    // Get all the sections.
    sections = $canvas.data('section');

    if ($canvas.data('image')) {
      context.drawImage($canvas.data('image'), 0, 0);
    }

    // Draw each section of steps.
    for (var j = 0; j < sections.stack.length; j++) {
      // Get this section.
      var stack = sections.stack[j]
      // Draw each step of the section.
      for(var i=0; i < stack.x.length; i++) {
        // Set our color.
        context.strokeStyle = stack.strokeStyle[i];
        // Set the pen size.
        context.lineWidth = stack.lineWidth[i];
        // Place the pen.
        context.beginPath();
        if (stack.drag[i] && i) {
          // If we're dragging, then do so.
          context.moveTo(stack.x[i-1], stack.y[i-1]);
        }
        else {
          // Otherwise, just draw a point.
          context.moveTo(stack.x[i]-1, stack.y[i]);
        }
        // Draw the line from that pos.
        context.lineTo(stack.x[i], stack.y[i]);
        // Lift the pen.
        context.closePath();
        // Make it so.
        context.stroke();
      }
    }
  }

  /**
   * Get the canvas HTML5 context.
   */
  opalCanvas.canvasContext = function(canvas) {
    // Account for IE.
    if(typeof G_vmlCanvasManager != 'undefined') {
      canvas = G_vmlCanvasManager.initElement(canvas);
    }
    return canvas.getContext("2d");
  }

  opalCanvas.save = function($canvas) {
    // Get the DOM object of the canvas.
    var canvas = $canvas.get(0);
    var canvasData = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
    $.ajax({
      beforeSend: function(XMLHttpRequest) {
        // Add a throbber to the OK button.
        opalCanvas.throbber($canvas, true);
      },
      dataType: 'json',
      cache: false,
      data: {
        image: canvasData
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        opalCanvas.throbber($canvas);
        alert(Drupal.t('Fatal error: !error', {'!error': errorThrown}));
      },
      success: function(data, textStatus, XMLHttpRequest) {
        opalCanvas.throbber($canvas);
        if (!data.status) {
          alert(data.error);
        }
        else {
          // Clear the data since we have a new base image.
          $('.opalcanvas-data').val('');
          // Display any messages.
          $('#opalCanvas-message').html(data.data);
          // Set the fid to the input textfield.
          $('#' + Drupal.settings.opalCanvas.element[0] + ' input.opalcanvas-fid').val(data.fid);
        }
      },
      type: 'POST',
      url: Drupal.settings.opalCanvas.postURL
    });
  }

  Drupal.behaviors.opalCanvasLoadEditor = function(context) {
    $('#node-form:not(.opalCanvasLoadEditor-processed)').each(function() {
      $(this).addClass('opalCanvasLoadEditor-processed')
        .bind('submit', function(e) {
          for (var _wrapper in Drupal.settings.opalCanvas.element) {
            var $canvas = $('#opalcanvas-' + _wrapper);

            // Pop a section of steps from the canvas stack.
            var stack = $canvas.data('section').stack.length;

            // Do we have a section to undo?
            if (stack) {
              // Get the DOM object of the canvas.
              var canvas = $canvas.get(0);
              var canvasData = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
              $('.opalcanvas-data').val(canvasData);
            }
          }
        })
    });
    for (var _wrapper in Drupal.settings.opalCanvas.element) {
      var _element = Drupal.settings.opalCanvas.element[_wrapper];
      $('#' + _element + ' input.opalcanvas-undo:not(.opalCanvasLoadEditor-processed)').each(function() {
        $(this).addClass('opalCanvasLoadEditor-processed')
          .data('canvas', '#opalcanvas-' + _wrapper)
          .bind('click', function(e) {
            opalCanvas.undo($($(this).data('canvas')));
            return false;
          })
      });
      $('#' + _element + ' input.opalcanvas-redo:not(.opalCanvasLoadEditor-processed)').each(function() {
        $(this).addClass('opalCanvasLoadEditor-processed')
          .data('canvas', '#opalcanvas-' + _wrapper)
          .bind('click', function(e) {
            opalCanvas.redo($($(this).data('canvas')));
            return false;
          })
      });
      $('#' + _element + ' input.opalcanvas-save:not(.opalCanvasLoadEditor-processed)').each(function() {
        $(this).addClass('opalcanvasLoadEditor-processed')
          .data('canvas', '#opalcanvas-' + _wrapper)
          .bind('click', function(e) {
            opalCanvas.save($($(this).data('canvas')));
            return false;
          })
      });
      $('#' + _element + ' input.opalcanvas-background-color:not(.opalCanvasLoadEditor-processed)').each(function() {
        $(this).addClass('opalcanvasLoadEditor-processed')
          .data('canvas', '#opalcanvas-' + _wrapper)
          .bind('blur', function(e) {
            $($(this).data('canvas')).css('background-color', $(this).val());
          })
      });
      $('#' + _element + ' input.opalcanvas-fid:not(.opalCanvasLoadEditor-processed)').each(function() {
        $(this).addClass('opalcanvasLoadEditor-processed')
          .val(Drupal.settings.opalCanvas.canvases[_wrapper].fid);
      });
    }
    for (var _canvasDiv in Drupal.settings.opalCanvas.canvases) {
      var $div = $('#' + Drupal.settings.opalCanvas.canvases[_canvasDiv].wrapper);
      var $canvas = $('<canvas></canvas>')
        .attr('id', 'opalcanvas-' + _canvasDiv);
      $div.not('.opalcanvasLoadEditor-processed')
        .addClass('opalcanvasLoadEditor-processed')
        .append($canvas);
      var _canvas = $('#opalcanvas-' + _canvasDiv).get(0);
      var img = new Image();
      img.canvas = $('#opalcanvas-' + _canvasDiv);
      img.onload = function() {
        img.canvas.data('image', img);
        opalCanvas.redraw(img.canvas);
      }
      img.src = Drupal.settings.opalCanvas.canvases[_canvasDiv].image;
      _canvas.setAttribute('width', $div.width());
      _canvas.setAttribute('height', $div.height());
      $('#opalcanvas-' + _canvasDiv)
        .mousedown(function(e){
          $(this).focus();
          var mouseX = e.pageX - this.offsetLeft;
          var mouseY = e.pageY - this.offsetTop;

          $(this).data('paint', true);
          $(this).data('section').newStack();
          var offset = $(this).offset();
          opalCanvas.addClick($(this), e.pageX - offset.left, e.pageY - offset.top);
          opalCanvas.redraw($(this));
        })
        .mousemove(function(e) {
          if ($(this).data('paint')) {
            var offset = $(this).offset();
            opalCanvas.addClick($(this), e.pageX - offset.left, e.pageY - offset.top, true);
            opalCanvas.redraw($(this));
          }

        })
        .mouseup(function(e){
          if ($(this).data('paint')) {

          }
          $(this).data('paint', false);
        })
        .mouseleave(function(e){
          if ($(this).data('paint')) {

          }
          $(this).data('paint', false);
        })
        .data('context', opalCanvas.canvasContext($('#opalcanvas-' + _canvasDiv).get(0)))
        .data('section', new opalCanvasSection())
        .data('undo', new opalCanvasSection())
        .data('paint', false);
      $(document).keypress(function(e) {
          switch(e.which) {
            case 121:
              // ^Y
              if (e.ctrlKey) {
                opalCanvas.redo($('#opalcanvas-0'));
              }
              break;
            case 122:
              // ^Z
              if (e.ctrlKey) {
                opalCanvas.undo($('#opalcanvas-0'));
              }
              break;
          }
        })
    }
  }

})(jQuery);
