
/**
 * @file opalcanvas/includes/js/opalcanvas.widget.js
 *
 * Widget dialog to popup canvas editor.
 */

(function($) {

  var opalCanvas = opalCanvas || {};
    // Define the opalCanvas object.
  Drupal.opalCanvas = Drupal.opalCanvas || {};

  // Our 'global' variable for the dialog jQuery object.
  Drupal.opalCanvas.$dialog = null;

  Drupal.opalCanvas.addedCSS = [];
  Drupal.opalCanvas.addedJS = [];

  Drupal.opalCanvas.originalSettings = null;
  /**
 * Save the page's original Drupal.settings.
 */
Drupal.opalCanvas.saveSettings = function() {
  if (!Drupal.opalCanvas.originalSettings) {
    Drupal.opalCanvas.originalSettings = Drupal.settings;
  }
};

/**
 * Restore the page's original Drupal.settings.
 */
Drupal.opalCanvas.restoreSettings = function() {
  Drupal.settings = Drupal.opalCanvas.originalSettings;
  Drupal.opalCanvas.originalSettings = null;
};

  Drupal.opalCanvas.saveSettings();

  Drupal.behaviors.opalCanvasWidgetLinkLaunch = function(context) {
    $('.opalcanvas-widget a:not(.opalCanvasWidgetLinkLaunch-processed)', context)
      .addClass('opalCanvasWidgetLinkLaunch-processed')
      .bind('click', function(e) {
        Drupal.opalCanvas.dialog($(this));
        return false;
      });
  }

  /**
   * Display the Drupal throbber.
   *
   * @param $element
   *  The jQuery element to append the throbber to.
   * @param boolean throbber
   *  If TRUE, then display the throbber element.
   */
  Drupal.opalCanvas.throbber = function($element, throbber) {
    if (throbber) {
      $element.after('<div class="ahah-progress ahah-progress-throbber"><div class="throbber">&nbsp;</div></div>');
    }
    else {
      $element.siblings('.ahah-progress').remove();
    }
  }

   /**
   * Helper function to build the browser.
   *
   * @param $link
   *  The jQuery object for the field link we're launching from.
   */
  Drupal.opalCanvas.dialog = function($link) {
    var element =  $link.data('element');
    var fieldName = $link.data('fieldName');
    // Create our media browser dialog.
    Drupal.opalCanvas.$dialog = $('<div id="opalCanvas-dialog"></div>')
      .dialog({
        autoopen: false,
        modal: true,
        width: 642,
        height: 'auto',
        title: 'OpalCanvas Image Editor'
      })
      // Remove the dialog entirely from the DOM after closing.
      .bind( "dialogclose", function(event, ui) {
        Drupal.opalCanvas.restoreSettings();
        $(this).remove();
      })

    // Grab the remote output for the browser.
    Drupal.opalCanvas.throbber(Drupal.opalCanvas.$dialog, true);
    $.ajax({
      beforeSend: function(XMLHttpRequest) {
        Drupal.opalCanvas.saveSettings();
      },
      type: 'POST',
      dataType: 'json',
      url: Drupal.settings.opalCanvas.ajaxURL,
      data: { js: true, field_name: fieldName },
      success: function(data, textStatus, XMLHttpRequest) {
        Drupal.opalCanvas.throbber(Drupal.opalCanvas.$dialog, false);
        if (data.status) {
          // Open the browser.
          Drupal.opalCanvas.$dialog.html(data.data).dialog('open');
          // Attach any required behaviors to the browser.
          console.log(data.js);
          Drupal.opalCanvas.addJS(data.js);
          Drupal.attachBehaviors(Drupal.opalCanvas.$dialog);
        }
        else {
          // Failure...
          Drupal.opalCanvas.restoreSettings();
          alert(Drupal.t('Unknown error in Drupal.opalCanvas.dialog.'));
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // Failure...
        Drupal.opalCanvas.restoreSettings();
        Drupal.opalCanvas.throbber(Drupal.opalCanvas.$dialog, false);
        alert(Drupal.t('Error in Drupal.opalCanvas.dialog: @error', { '@error': textStatus }));
      }
    });
  }

/**
 * Add additional CSS to the page.
 */
Drupal.opalCanvas.addCSS = function(css) {
  Drupal.opalCanvas.addedCSS = [];
  for (var type in css) {
    for (var file in css[type]) {
      var link = css[type][file];
      // Does the page already contain this stylesheet?
      if (!$('link[href='+ $(link).attr('href') + ']').length) {
        $('head').append(link);
        Drupal.opalCanvas.addedCSS.push(link); // Keep a list, so we can remove them later.
      }
    }
  }
};

/**
 * Add additional Javascript to the page.
 */
Drupal.opalCanvas.addJS = function(js) {
  // Parse the json info about the new context.
  var scripts = [];
  var inlines = [];
  for (var type in js) {
    if (type != 'setting') {
      for (var file in js[type]) {
        if (type == 'inline') {
          inlines.push($(js[type][file]).text());
        }
        else {
          scripts.push($(js[type][file]).attr('src'));
        }
      }
    }
  }

  // Add new JS settings to the page, needed for #ahah properties to work.
  Drupal.settings = js.setting;
  Drupal.settings.verticalTabs = Drupal.settings.verticalTabs || {};
// console.log(Drupal.settings);
//   for (var i in scripts) {
//     var src = scripts[i];
//     if (!$('script[src='+ src + ']').length && !Drupal.opalCanvas.addedJS[src]) {
//       console.log(src);
//       // Get the script from the server and execute it.
//       $.ajax({
//         type: 'GET',
//         url: src,
//         dataType: 'script',
//         async : false,
//         success: function(script) {
//           eval(script);
//         }
//       });
//       // Mark the js as added to the underlying page.
//       Drupal.opalCanvas.addedJS[src] = true;
//     }
//   }

  return inlines;
};

/**
 * Execute the jit loaded inline scripts.
 * Q: Do we want to re-excute the ones already in the page?
 *
 * @param inlines
 *   Array of inline scripts.
 */
Drupal.opalCanvas.addInlineJS = function(inlines) {
  // Load the inlines into the page.
  for (var n in inlines) {
    // If the script is not already in the page, execute it.
    if (!$('script:not([src]):contains(' + inlines[n] + ')').length) {
      eval(inlines[n]);
    }
  }
};

})(jQuery);
