<?php

/**
 * @file media_webcam/includes/themes/media-webcam-widget.tpl.php
 *
 * Template file for theme('media_webcam_widget').
 *
 * Available variables:
 */
?>
<div id="media-webcam-widget-<?php print $id; ?>" class="media-webcam-widget">
  <?php if ($title) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php if ($alternate_content) : ?>
    <p><?php print $alternate_content; ?></p>
  <?php endif; ?>
  <?php if ($get_flash_link) : ?>
    <p><?php print $get_flash_link; ?></p>
  <?php endif; ?>
</div>
