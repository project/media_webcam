<?php

/**
 *  @file
 *  Theme functions for the Media: Webcam module.
 */

/**
 * Preprocess for theme('media_webcam_widget').
 */
function template_preprocess_media_webcam_widget(&$variables) {
  $variables['field'] = content_fields($variables['element']['#field_name']);
  $variables['title'] = t('@title', array('@title' => media_webcam_variable_get('title')));
  $varibles['alternate_content'] = t('@alternate_content', array('@alternate_content' => media_webcam_variable_get('alternate_content')));
  $variables['get_flash_alt'] = t('@get_flash_alt', array('@get_flash_alt' => media_webcam_variable_get('get_flash_alt')));
  $variables['get_flash_image'] = theme('image', media_webcam_variable_get('get_flash_image'), $variables['get_flash_alt'], $variables['get_flash_alt'], NULL, FALSE);
  $variables['get_flash_link'] = l($variables['get_flash_image'], media_webcam_variable_get('get_flash_url'), array('html' => TRUE));
  $path = drupal_get_path('module', 'media_webcam');
  $expressInstallPath = url($path .'/src/webcam/bin/expressInstall.swf');
  $webcamPath = url($path .'/src/webcam/bin/webcam.swf');
  $id = $variables['id'];
  $js = <<<JS
    var flashvars = {
    };
    var params = {
      menu: "false",
      scale: "noScale",
      allowFullscreen: "true",
      allowScriptAccess: "always",
      bgcolor: "#FFFFFF"
    };
    var attributes = {
      id:"media-webcam-widget-$id"
    };
    swfobject.embedSWF("$webcamPath", "media-webcam-widget-$id", "800", "600", "10.0.0", "$expressInstallPath", flashvars, params, attributes);
JS;
  drupal_add_js($path .'/src/webcam/bin/js/swfobject.js');
  drupal_add_js($js, 'inline');
  drupal_add_css($path .'/includes/themes/css/media-webcam.css');
}
